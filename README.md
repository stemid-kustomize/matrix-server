# Synapse server setup in kubernetes with Kustomize

This is my personal Synapse setup but anyone can try to use it if they're feeling brave.

## Kustomize overlays

* [sydit](https://gitlab.com/stemid-kustomize/matrix-server/-/tree/sydit) can be used as an example overlay for your own.
* Copy ``kustomize/overlays/sydit`` to your own overlay, create your own private branch and add a CI pipeline if you want.

## Generate synapse config and key

    podman run -it --rm --mount type=volume,src=synapse-data,dst=/data -e SYNAPSE_SERVER_NAME=mtrx.chat -e SYNAPSE_REPORT_STATS=no matrixdotorg/synapse:latest generate
    ls -lat $(podman volume inspect synapse-data|jq '.[0].Mountpoint'|tr -d '"')
    # podman volume rm synapse-data

Copy them into your overlay dirs where appropriate, see the sydit overlay for an example in ``kustomization.yaml``.

## Delegation

Hosting in Kubernetes you most likely will not expose port TCP/8448 to the synapse API so you need [delegation](https://github.com/matrix-org/synapse/blob/develop/docs/delegate.md) for other instances to find yours and federate with it.

In most circumstances this is done with a .well-known directory but in kubernetes I prefer DNS.

In my case I use an SRV record called _matrix._tcp.mtrx.chat with ``<priority> <weight> <port> <host>.`` as value. Priority and weight can be 1 and 10, not sure what that means but it works. Port and host are 443 and mtrx.chat respectively.
