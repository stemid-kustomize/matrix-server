#!/usr/bin/env bash

ctr=$(buildah from alpine:3.13)
buildah run "$ctr" apk add -Uu darkhttpd
buildah config --author 'Stefan Midjich (stefan at sydit punkt se)' "$ctr"
buildah config --port 80 "$ctr"
buildah config --entrypoint '["/usr/bin/darkhttpd", "/var/www", "--chroot"]' "$ctr"
buildah commit "$ctr" localhost/darkhttpd:latest
